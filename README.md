# Tormenta20

## Descrição
Este é um sistema NÃO OFICIAL feito e mantido por fãs, sem qualquer afiliação a Tormenta20 ou a Jambo Editora.
Tormenta 20 é um RPG brasileiro e pertence a Jambo Editora.


## Colaboradores
* André Oliveira
* Gustavo Reis
* TheTruePortal
* Mateus Marochi
* Matheus Clemente
* Roberto Caetano
* Victor Kullack
* Alexandre Galdino
* Vinicius Lima Silva

## Atribuições
- [FoundryVTT](https://github.com/FoundryVTT/) pelo módulo [dnd5e](https://github.com/FoundryVTT/dnd5e), cujo código foi adaptado neste sistema.
- [sdenec](https://github.com/sdenec/) pelo módulo [Tidy5e Sheet](https://github.com/sdenec/tidy5e-sheet), cujo código foi adaptado neste sistema.
- Este sistema usa artes de tokens do [2 Minute TableTop](https://2minutetabletop.com/).